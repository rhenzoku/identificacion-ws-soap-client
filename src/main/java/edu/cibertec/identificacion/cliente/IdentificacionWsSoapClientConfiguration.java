package edu.cibertec.identificacion.cliente;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import edu.cibertec.identificacion.cliente.ws.PersonaClient;

@Configuration
public class IdentificacionWsSoapClientConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("edu.cibertec.identificacion.cliente.dto");
		return marshaller;
	}

	@Bean
	public PersonaClient personaClient(Jaxb2Marshaller marshaller) {
		PersonaClient client = new PersonaClient();
		client.setDefaultUri("http://localhost:9090/identificacion-ws-soap/ws/identificacion");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}
