//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.03.10 a las 12:06:39 AM COT 
//


package edu.cibertec.identificacion.cliente.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para mejorHuella complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="mejorHuella"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mano" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dedo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mejorHuella", propOrder = {
    "mano",
    "dedo"
})
public class MejorHuella {

    @XmlElement(required = true)
    protected String mano;
    @XmlElement(required = true)
    protected String dedo;

    /**
     * Obtiene el valor de la propiedad mano.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMano() {
        return mano;
    }

    /**
     * Define el valor de la propiedad mano.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMano(String value) {
        this.mano = value;
    }

    /**
     * Obtiene el valor de la propiedad dedo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDedo() {
        return dedo;
    }

    /**
     * Define el valor de la propiedad dedo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDedo(String value) {
        this.dedo = value;
    }

}
