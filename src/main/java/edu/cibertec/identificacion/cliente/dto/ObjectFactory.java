//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.03.10 a las 12:06:39 AM COT 
//


package edu.cibertec.identificacion.cliente.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the edu.cibertec.identificacion.cliente.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: edu.cibertec.identificacion.cliente.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MejorHuellaRequest }
     * 
     */
    public MejorHuellaRequest createMejorHuellaRequest() {
        return new MejorHuellaRequest();
    }

    /**
     * Create an instance of {@link MejorHuellaResponse }
     * 
     */
    public MejorHuellaResponse createMejorHuellaResponse() {
        return new MejorHuellaResponse();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link ValidarIdentidadRequest }
     * 
     */
    public ValidarIdentidadRequest createValidarIdentidadRequest() {
        return new ValidarIdentidadRequest();
    }

    /**
     * Create an instance of {@link ValidarIdentidadResponse }
     * 
     */
    public ValidarIdentidadResponse createValidarIdentidadResponse() {
        return new ValidarIdentidadResponse();
    }

    /**
     * Create an instance of {@link ListaMejoresHuellas }
     * 
     */
    public ListaMejoresHuellas createListaMejoresHuellas() {
        return new ListaMejoresHuellas();
    }

    /**
     * Create an instance of {@link MejorHuella }
     * 
     */
    public MejorHuella createMejorHuella() {
        return new MejorHuella();
    }

}
