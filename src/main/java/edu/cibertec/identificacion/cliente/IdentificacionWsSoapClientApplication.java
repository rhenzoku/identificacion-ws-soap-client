package edu.cibertec.identificacion.cliente;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import edu.cibertec.identificacion.cliente.dto.ListaMejoresHuellas;
import edu.cibertec.identificacion.cliente.dto.MejorHuella;
import edu.cibertec.identificacion.cliente.dto.MejorHuellaResponse;
import edu.cibertec.identificacion.cliente.dto.ValidarIdentidadResponse;
import edu.cibertec.identificacion.cliente.ws.PersonaClient;

@SpringBootApplication
public class IdentificacionWsSoapClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(IdentificacionWsSoapClientApplication.class, args);
	}

	//probamos que se consuma el servicio soap correctamente
	@Bean
	CommandLineRunner lookup(PersonaClient client) {
		return args -> {
			String dni = "34343432";
			String huellaDerecha = "SFVFTExBSU5ESUNFREVSRUNITzcwMDgyNDEy";
			String huellaIzquierda = "SFVFTExBSU5ESUNFSVpRVUlFUkRPNzAwODI0MTI=";

			if (args.length > 0) {
				dni = args[0];
			}
			
			//Probando el metodo conseguirMejorHuella
			MejorHuellaResponse response = client.conseguirMejorHuella(dni);
			if(response.getPersona() != null) {
				ListaMejoresHuellas listaMejoresHuellas = response.getPersona().getListaMejoresHuellas();
				System.out.println(":: Mejores huellas");
				for (MejorHuella mejorHuella: listaMejoresHuellas.getMejorHuella()) {
					System.out.println("Mano: " + mejorHuella.getMano() + ", mejor huella: " + mejorHuella.getDedo());
				}
			}
			
			//Probando el metodo validarIdentidad
			ValidarIdentidadResponse validarIdentidadResponse = client.validarIdentidad(dni, huellaDerecha, huellaIzquierda);
			System.out.println("La persona enviada es: " + validarIdentidadResponse.isValido());
			
		};

	}
}
