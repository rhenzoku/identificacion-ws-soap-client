package edu.cibertec.identificacion.cliente.ws;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import edu.cibertec.identificacion.cliente.dto.MejorHuellaRequest;
import edu.cibertec.identificacion.cliente.dto.MejorHuellaResponse;
import edu.cibertec.identificacion.cliente.dto.Persona;
import edu.cibertec.identificacion.cliente.dto.ValidarIdentidadRequest;
import edu.cibertec.identificacion.cliente.dto.ValidarIdentidadResponse;

public class PersonaClient extends WebServiceGatewaySupport {

	private static final Logger log = LogManager.getLogger(PersonaClient.class);

	  public MejorHuellaResponse conseguirMejorHuella(String dni) {

		MejorHuellaRequest request = new MejorHuellaRequest();
	    request.setDni(dni);

	    log.info("Enviando petición conseguirMejorHuella para el dni: " + dni);

	    MejorHuellaResponse response = (MejorHuellaResponse) getWebServiceTemplate()
	        .marshalSendAndReceive("http://localhost:9090/identificacion-ws-soap/ws/identificacion", request,
	            new SoapActionCallback(
	                "http://identificacion.cibertec.edu/identificacion/MejorHuellaRequest"));

	    return response;
	  }
	  
	  public ValidarIdentidadResponse validarIdentidad(String dni, String huellaDerecha, String huellaIzquierda) {

		  	ValidarIdentidadRequest request = new ValidarIdentidadRequest();
		  	Persona persona = new Persona();
		  	persona.setDni(dni);
		  	persona.setHuellaDerecha(huellaDerecha);
		  	persona.setHuellaIzquierda(huellaIzquierda);
		  	request.setPersona(persona);

		    log.info("Enviando petición validarIdentidad para la persona: " + persona);

		    ValidarIdentidadResponse response = (ValidarIdentidadResponse) getWebServiceTemplate()
		        .marshalSendAndReceive("http://localhost:9090/identificacion-ws-soap/ws/identificacion", request,
		            new SoapActionCallback(
		                "http://identificacion.cibertec.edu/identificacion/ValidarIdentidadRequest"));

		    return response;
		  }
}
